using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Andar : MonoBehaviour
{
    public float vel = 0.01f;

    public float salto = 0.1f;
    public float JumpFrecuency;
    private float lastJump;
    private Rigidbody2D rb;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.A))
        {
            transform.position += Vector3.left * vel;

        }
        if (Input.GetKey(KeyCode.D))
        {
            transform.position += Vector3.right * vel;
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (Time.time - lastJump >= JumpFrecuency)
            {
                rb.velocity = Vector2.up * salto;
                lastJump = Time.time;
            }
        }

        //if (Input.GetKey(KeyCode.DownArrow))
        //{
        //    transform.position += Vector3.left * vel;
        //}
    }
}
